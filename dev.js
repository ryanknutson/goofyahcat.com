const path = require('node:path')
const express = require('express')
const app = express()
const port = 3737

app.set('view engine', 'pug')
app.set('views', path.join(__dirname, 'src', 'views'))

app.use('/', express.static(path.join(__dirname, 'public')))

app.get('/', (req, res) => {
  res.render('index')
})

app.listen(port, () => {
  console.log(`goofy ah cat is inside your computer on port ${port}`)
})
